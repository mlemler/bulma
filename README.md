# mlemler.de - Styled with Bulma

My private homepage styled with [Bulma](https://bulma.io/).

## Getting started

### Prerequisites
* [Node.js / npm](https://nodejs.org/en/download/)

### Installing
To install all needed dependencies run
```
npm install
```

## Development

### Build CSS
To build the `css/main.css` file from the source at `_sass/main.scss` run
```
npm run css-build
```
You can also start a watch task which will automatically rebuild the `css/main.css` after every change at the
`_sass/main.scss` by running
```
npm run css-watch
```

## Used Tools and Technologies
... in historical order.

### The CSS framework
#### The choice
First decision was to use a CSS Framework, so I don't have to think about the design too much. Just use what the
framework provides. I was thinking about [Bootstrap](https://getbootstrap.com/), [Foundation](https://get.foundation/)
and [Bulma](https://bulma.io) and decided to try the last.
* [Bulma](https://bulma.io) - the CSS framework I wanted to use

#### The installation
How do I get Bulma into my project? The [recommended](https://bulma.io/documentation/overview/start/) way is to either
use a CDN (Content delivery network) to import it as single CSS file into your project. Or use npm to get Bulma as
[Sass](https://sass-lang.com/) library.

Since I like it more to deliver all code from my side of the project and import as little as possible from foreign
systems I decided to use the npm way.
* [npm](https://www.npmjs.com/) - recommended Tool to install / use Bulma
* [Node.js](https://nodejs.org/en/) - recommended Tool to install npm

#### The customisation
The Bulma documentation provides three ways to [customization](https://bulma.io/documentation/customize/), with
`node-sass`, `Sass CLI` or `webpack`. All three ways force me to create a Sass file where I then can overwrite the
styles. So okay, I will use Sass/SCSS.
* [Sass/SCSS](https://sass-lang.com/) - the CSS preprocessor Bulma uses

Since no web browser understands SCSS I have to use one of the tools above to create my final CSS. And since I have
`node` already in use I tried the `node-sass` way like the Bulma documentation still proposes (August 2022). But
unfortunately [node-sass is deprecated](https://www.npmjs.com/package/node-sass) since October 2020. So I switched to
the recommended Dart Sass.
* [Dart Sass](https://sass-lang.com/dart-sass) - node module (npm package) to compile .scss files to CSS

##### How to name that color
The documentation hints to give the variables good names. But what name to take? What if I want to use two kinds of
blue? Luckily I came once across a page having a solution for this:
[Name that Color](http://chir.ag/projects/name-that-color/#1B92E8). And there is also a npm package for this task:
[name-that-color](https://www.npmjs.com/package/name-that-color). This way I came up with the names of my colors.
